package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceContextService;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class ProjectEndpointTest {

    @NotNull
    private final IServiceContextRepository serviceLocatorRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceLocator = new ServiceContextService(serviceLocatorRepository);

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final IProjectUserService projectService = serviceLocator.getProjectService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final IAdapterContextService adapterService = serviceLocator.getAdapterService();

    @NotNull
    private final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testAddProject for addProject(session, {1})")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testAddProject(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO addProject = this.projectEndpoint.addProject(
//                conversion, project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProject);
//        Assert.assertEquals(addRecord.getId(), addProject.getUserId());
//        Assert.assertEquals(project.getTitle(), addProject.getTitle());
//        Assert.assertEquals(project.getDescription(), addProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateByIndex for updateProjectByIndex(session, 0)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testUpdateProjectByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final String newDescription = UUID.randomUUID().toString();
//        Assert.assertNotNull(newDescription);
//        project.setTitle(newDescription);
//        @Nullable final ProjectDTO updateProject =
//                this.projectEndpoint.updateProjectByIndex(conversion, 0, project.getTitle(), newDescription);
//        Assert.assertNotNull(updateProject);
//        Assert.assertEquals(addProjectRecord.getId(), updateProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), updateProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), updateProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), updateProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateProjectById for updateProjectById((session, {1})")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testUpdateProjectById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final String newDescription = UUID.randomUUID().toString();
//        Assert.assertNotNull(newDescription);
//        project.setTitle(newDescription);
//        @Nullable final ProjectDTO updateProject = this.projectEndpoint.updateProjectById(
//                conversion, addProjectRecord.getId(), project.getTitle(), newDescription
//        );
//        Assert.assertNotNull(updateProject);
//        Assert.assertEquals(addProjectRecord.getId(), updateProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), updateProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), updateProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), updateProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteProjectById for deleteProjectById(session, 0)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteProjectById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectById(conversion, addProjectRecord.getId());
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), deleteProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteProjectByIndex for deleteProjectByIndex(session, 0)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteProjectByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectByIndex(conversion, 0);
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), deleteProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteProjectByTitle for deleteProjectByTitle(session, {1})")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteProjectByTitle(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectByTitle(conversion, addProjectRecord.getTitle());
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), deleteProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAllProjects for deleteAllProjects(session)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteAllProjects(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final Collection<ProjectDTO> deleteProjects = this.projectEndpoint.deleteAllProjects(conversion);
//        Assert.assertNotNull(deleteProjects);
//        Assert.assertNotEquals(0, deleteProjects.size());
//        final boolean isUserTasks = deleteProjects.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }
//
//    @Test
//    @TestCaseName("Run testGetProjectById for getProjectById(session, {1})")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetProjectById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO getProject = this.projectEndpoint.getProjectById(conversion, addProjectRecord.getId());
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), getProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetProjectByIndex for getProjectByIndex(session, {1})")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetProjectByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO getProject = this.projectEndpoint.getProjectByIndex(conversion, 0);
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), getProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteProjectByTitle for getProjectByTitle(session, 0)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetProjectByTitle(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final ProjectDTO getProject = this.projectEndpoint.getProjectByTitle(conversion, addProjectRecord.getTitle());
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
//        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProjectRecord.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProjectRecord.getDescription(), getProject.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteProjectByTitle for getAllProjects(session)")
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetAllProjects(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.projectEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addRecord);
//        @NotNull final Project addProjectRecord = this.projectService.add(
//                addRecord.getId(), project.getTitle(), project.getDescription()
//        );
//        Assert.assertNotNull(addProjectRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                user.getLogin(), user.getPasswordHash()
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final Collection<ProjectDTO> getAllProjects = this.projectEndpoint.getAllProjects(conversion);
//        Assert.assertNotNull(getAllProjects);
//        Assert.assertNotEquals(0, getAllProjects.size());
//        final boolean isUserTasks = getAllProjects.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }

}