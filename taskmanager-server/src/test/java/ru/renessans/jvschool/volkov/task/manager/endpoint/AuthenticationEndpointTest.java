package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceContextService;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class AuthenticationEndpointTest {

    @NotNull
    private final IServiceContextRepository serviceLocatorRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceLocator = new ServiceContextService(serviceLocatorRepository);

    @NotNull
    private final IAuthenticationEndpoint authEndpoint = new AuthenticationEndpoint(serviceLocator);

//    @Before
//    public void loadConfigurationBefore() {
//        this.serviceLocator.getConfigurationService().load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testSignUpUser for signUpUser(\"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testSignUpUser(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.authEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//
//        @Nullable final UserDTO addUser = this.authEndpoint.signUpUser(null, login, password);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(addUser.getId(), addUser.getId());
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(addUser.getRole(), addUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testGetUserRole for getUserRole(session)")
//    public void testGetUserRole() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.authEndpoint);
//        @NotNull final IConfigurationService configService = this.serviceLocator.getConfigurationService();
//        Assert.assertNotNull(configService);
//        configService.load();
//        @NotNull final IUserService userService = this.serviceLocator.getUserService();
//        Assert.assertNotNull(userService);
//        @NotNull final User addRecord = userService.addUser(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        @NotNull final ISessionService sessionService = this.serviceLocator.getSessionService();
//        Assert.assertNotNull(sessionService);
//        @NotNull final Session open = sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//
//        @NotNull final ISessionAdapterService sessionAdapter = this.serviceLocator.getAdapterService().getSessionAdapter();
//        Assert.assertNotNull(sessionAdapter);
//        @Nullable final SessionDTO conversion = sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final UserRole getRole = this.authEndpoint.getUserRole(conversion);
//        Assert.assertNotNull(getRole);
//        Assert.assertEquals(addRecord.getRole(), getRole);
//    }

}