package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalGetProjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.persistence.EntityManager;
import java.util.Objects;

public final class TaskUserService extends AbstractUserOwnerService<Task, ITaskUserRepository> implements ITaskUserService {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    public TaskUserService(
            @NotNull final IEntityManagerFactoryService managerFactoryService
    ) {
        this.managerFactoryService = managerFactoryService;
    }

    @NotNull
    @Override
    protected ITaskUserRepository createRepository() {
        return new TaskUserRepository(this.managerFactoryService.getEntityManager());
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final Task task
    ) {
        if (Objects.isNull(task)) throw new InvalidTaskException();
        return super.beginTransactionForResult(repository -> persist(task));
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String projectTitle,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(projectTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        @Nullable final Project project = projectRepository.getByTitle(userId, projectTitle);
        if (Objects.isNull(project)) throw new IllegalGetProjectException();

        @NotNull final Task task = new Task(userId, title, description);
        task.setProject(project);

        return add(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return add(task);
    }

}