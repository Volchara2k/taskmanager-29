package ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidSignatureSaltException extends AbstractException {

    @NotNull
    private static final String EMPTY_SALT = "Ошибка! Параметр \"соль для подписи\" является пустым или null!\n";

    public InvalidSignatureSaltException() {
        super(EMPTY_SALT);
    }

}