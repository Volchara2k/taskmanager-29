package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.*;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    public UserService(
            @NotNull final IEntityManagerFactoryService managerFactoryService
    ) {
        this.managerFactoryService = managerFactoryService;
    }

    @NotNull
    @Override
    protected IUserRepository createRepository() {
        return new UserRepository(this.managerFactoryService.getEntityManager());
    }

    @Nullable
    @SneakyThrows
    @Override
    public User getUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return super.beginTransactionForResult(repository -> repository.getById(id));
    }

    @Nullable
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        return super.beginTransactionForResult(repository -> repository.getByLogin(login));
    }

    @NotNull
    @Override
    public UserRole getUserRole(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) return UserRole.UNKNOWN;
        @Nullable final User user = getUserById(userId);
        if (Objects.isNull(user)) return UserRole.UNKNOWN;
        return user.getRole();
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash);
        return super.beginTransactionForResult(repository -> persist(user));
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return super.persist(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole userRole
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(userRole)) throw new InvalidUserRoleException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, userRole);
        return super.persist(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User updatePasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new InvalidPasswordException();
        @NotNull final String passwordHash = HashUtil.getSaltHashLine(newPassword);

        @Nullable final User user = getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setPasswordHash(passwordHash);

        return super.merge(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        @Nullable final User user = getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        return super.merge(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new InvalidLastNameException();

        @Nullable final User user = getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return super.merge(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        user.setLockdown(true);
        return super.merge(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        user.setLockdown(false);
        return super.merge(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteUserById(
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidUserIdException();
        @Nullable final User user = getUserById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        return super.beginTransactionForResult(repository -> repository.deleteById(id));
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        return super.beginTransactionForResult(repository -> repository.deleteByLogin(login));
    }

    @NotNull
    @Override
    public Collection<User> getAllRecords() {
        return super.beginTransactionForResult(IUserRepository::getAllRecords);
    }

    @NotNull
    @Override
    public Collection<User> initialDemoData() {
        return Arrays.asList(
                super.merge(
                        new User(
                                DemoDataConst.USER_TEST_ID, DemoDataConst.USER_TEST_LOGIN,
                                HashUtil.getSaltHashLine(DemoDataConst.USER_TEST_PASSWORD)
                        )
                ),
                super.merge(
                        new User(
                                DemoDataConst.USER_DEFAULT_ID, DemoDataConst.USER_DEFAULT_LOGIN,
                                HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD)
                        )
                ),
                super.merge(
                        new User(
                                DemoDataConst.USER_ADMIN_ID, DemoDataConst.USER_ADMIN_LOGIN,
                                HashUtil.getSaltHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                                UserRole.ADMIN
                        )
                ),
                super.merge(
                        new User(
                                DemoDataConst.USER_MANAGER_ID, DemoDataConst.USER_MANAGER_LOGIN,
                                HashUtil.getSaltHashLine(DemoDataConst.USER_MANAGER_PASSWORD)
                        )
                )
        );
    }

}