package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public User getById(@NotNull final String id) {
        @NotNull final String jpqlQuery = "FROM User WHERE id = :id";
        @NotNull final List<User> users = this.entityManager.createQuery(jpqlQuery, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User getByLogin(@NotNull final String login) {
        @NotNull final String jpqlQuery = "FROM User WHERE login = :login";
        @NotNull final List<User> users = this.entityManager.createQuery(jpqlQuery, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteById(@NotNull final String id) {
        @Nullable final User user = getById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        this.entityManager.remove(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteByLogin(@NotNull final String login) {
        @Nullable final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        this.entityManager.remove(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<User> getAllRecords() {
        @NotNull final String jpqlQuery = "FROM User";
        return this.entityManager.createQuery(jpqlQuery, User.class)
                .getResultList();
    }

}