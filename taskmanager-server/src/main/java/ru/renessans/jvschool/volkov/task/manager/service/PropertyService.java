package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalConfigurationLoadingException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.InvalidPropertyFetchException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidPropertyKeyException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidPropertyNameException;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String DEFAULT_CONFIG_SOURCE = "/configuration.properties";

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void load() {
        loadProperties(DEFAULT_CONFIG_SOURCE);
    }

    @SneakyThrows
    @Override
    public void load(
            @Nullable final String name
    ) {
        if (ValidRuleUtil.isNullOrEmpty(name)) throw new InvalidPropertyNameException();
        loadProperties(name);
    }

    @NotNull
    @SneakyThrows
    @Override
    public String getStringProperty(
            @Nullable final String key
    ) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new InvalidPropertyKeyException();
        return fetchPropertyAsString(key);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Integer getIntegerProperty(
            @Nullable final String key
    ) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new InvalidPropertyKeyException();
        @NotNull final String integerLine = fetchPropertyAsString(key);
        return Integer.parseInt(integerLine);
    }

    @SneakyThrows
    private void loadProperties(
            @NotNull final String name
    ) {
        @NotNull @Cleanup final InputStream inputStream = getClass().getResourceAsStream(name);
        try {
            this.properties.load(inputStream);
        } catch (@NotNull final Exception exception) {
            throw new IllegalConfigurationLoadingException(exception.getCause());
        }
    }

    @NotNull
    @SneakyThrows
    private String fetchPropertyAsString(
            @Nullable final String key
    ) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new InvalidPropertyKeyException();
        @Nullable final String valueFromProperty = this.properties.getProperty(key);
        @Nullable final String valueFromEnvironment = System.getProperty(key);
        if (Objects.isNull(valueFromProperty) && Objects.isNull(valueFromEnvironment))
            throw new InvalidPropertyFetchException();
        if (!Objects.isNull(valueFromEnvironment)) return valueFromEnvironment;
        return valueFromProperty;
    }

}