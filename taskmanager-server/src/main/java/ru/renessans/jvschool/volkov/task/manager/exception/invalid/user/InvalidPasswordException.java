package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPasswordException extends AbstractException {

    @NotNull
    private static final String EMPTY_PASSWORD = "Ошибка! Параметр \"пароль\" является пустым или null!\n";

    public InvalidPasswordException() {
        super(EMPTY_PASSWORD);
    }

}