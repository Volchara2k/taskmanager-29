package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.SessionRepository;
import ru.renessans.jvschool.volkov.task.manager.util.SignatureUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

public final class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    @NotNull
    private final IAuthenticationService authService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IConfigurationService configService;

    public SessionService(
            @NotNull final IEntityManagerFactoryService managerFactoryService,
            @NotNull final IAuthenticationService authService,
            @NotNull final IUserService userService,
            @NotNull final IConfigurationService configService
    ) {
        this.managerFactoryService = managerFactoryService;
        this.authService = authService;
        this.userService = userService;
        this.configService = configService;
    }

    @NotNull
    @Override
    protected ISessionRepository createRepository() {
        return new SessionRepository(this.managerFactoryService.getEntityManager());
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session setSignature(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        session.setSignature(null);
        @NotNull final String salt = this.configService.getSessionSalt();
        @NotNull final Integer cycle = this.configService.getSessionCycle();
        @NotNull final String signature = SignatureUtil.getHashSignature(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session openSession(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();

        @NotNull final User user = validateUserData(login, password);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Session signatureSession = setSignature(session);

        return super.beginTransactionForResult(repository -> repository.persist(signatureSession));
    }

    @Nullable
    @SneakyThrows
    @Override
    public Session closeSession(
            @Nullable final Session session
    ) {
        @Nullable final Session validate = validateSession(session);
        if (Objects.isNull(validate.getUserId()))
            throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        @Nullable final Session open = createRepository().getById(validate.getId());
        return super.deletedRecord(open);
    }

    @Override
    public boolean closeAllSessions(
            @Nullable final Session session
    ) {
        validateSession(session);
        return super.deletedAllRecords();
    }

    @Nullable
    @SneakyThrows
    @Override
    public Session getSessionByUserId(
            @Nullable final Session session
    ) {
        @NotNull final Session validate = validateSession(session);
        if (Objects.isNull(validate.getUserId()))
            throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        return super.beginTransactionForResult(repository -> repository.getSessionByUserId(validate.getUserId()));
    }

    @Nullable
    @SneakyThrows
    @Override
    public Session closeSessionByUserId(
            @Nullable final Session session
    ) {
        @Nullable final Session validate = validateSession(session);
        if (Objects.isNull(validate.getUserId()))
            throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());

        @Nullable final Session open = createRepository().getSessionByUserId(validate.getUserId());
        if (Objects.isNull(open)) return null;

        return super.deletedRecord(open);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User validateUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        @NotNull final UserDataValidState authState = verifyValidUserData(login, password);
        if (authState.isNotSuccess()) throw new AccessFailureException(authState.getTitle());
        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) throw new AccessFailureException(UserDataValidState.USER_NOT_FOUND.getTitle());
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserDataValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        return this.authService.verifyValidUserData(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session validateSession(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        @NotNull final SessionValidState sessionState = verifyValidSessionState(session);
        if (sessionState.isNotSuccess()) throw new AccessFailureException(sessionState.getTitle());
        return session;
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session validateSession(
            @Nullable final Session session,
            @Nullable final UserRole requiredRole
    ) {
        if (Objects.isNull(session)) throw new AccessFailureException(SessionValidState.NO_SESSION.getTitle());
        @NotNull final SessionValidState sessionState = verifyValidSessionState(session);
        if (sessionState.isNotSuccess()) throw new AccessFailureException(sessionState.getTitle());
        @NotNull final PermissionValidState permissionValidState = verifyValidPermissionState(session, requiredRole);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());
        return session;
    }

    @NotNull
    @Override
    public SessionValidState verifyValidSessionState(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) return SessionValidState.NO_SESSION;
        if (ValidRuleUtil.isNullOrEmpty(session.getUserId())) return SessionValidState.NO_USER_ID;
        if (ValidRuleUtil.isNullOrEmpty(session.getTimestamp())) return SessionValidState.NO_TIMESTAMP;
        if (ValidRuleUtil.isNullOrEmpty(session.getSignature())) return SessionValidState.NO_SIGNATURE;

        @Nullable final Session temp = session.clone();
        if (Objects.isNull(temp)) return SessionValidState.NO_SESSION;

        @NotNull final String signatureSrc = session.getSignature();
        @Nullable final String signatureTrg = setSignature(temp).getSignature();
        final boolean isEqualSignatures = signatureSrc.equals(signatureTrg);
        if (!isEqualSignatures) return SessionValidState.DIFFERENT_SIGNATURES;

        @NotNull final ISessionRepository sessionRepository = createRepository();
        if (!sessionRepository.containsUserId(session.getUserId())) return SessionValidState.SESSION_CLOSED;

        return SessionValidState.SUCCESS;
    }

    @NotNull
    @Override
    public PermissionValidState verifyValidPermissionState(
            @Nullable final Session session,
            @Nullable final UserRole commandRole
    ) {
        if (Objects.isNull(session)) return PermissionValidState.NO_ACCESS_RIGHTS;
        return this.authService.verifyValidPermission(session.getUserId(), commandRole);
    }

    @NotNull
    @Override
    public Collection<Session> getAllRecords() {
        return super.beginTransactionForResult(ISessionRepository::getAllRecords);
    }

}