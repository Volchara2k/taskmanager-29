package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidValueException extends AbstractException {

    @NotNull
    private static final String EMPTY_VALUE = "Ошибка! Параметр \"значение\" является пустым или null!\n";

    public InvalidValueException() {
        super(EMPTY_VALUE);
    }

}