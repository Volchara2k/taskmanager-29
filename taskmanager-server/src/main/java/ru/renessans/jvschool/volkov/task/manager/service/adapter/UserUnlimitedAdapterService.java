package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserUnlimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.UserUnlimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Objects;

public final class UserUnlimitedAdapterService implements IUserUnlimitedAdapterService {

    @Nullable
    @Override
    public UserUnlimitedDTO toDTO(@Nullable final User convertible) {
        if (Objects.isNull(convertible)) return null;
        return UserUnlimitedDTO.builder()
                .id(convertible.getId())
                .login(convertible.getLogin())
                .passwordHash(convertible.getPasswordHash())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .role(convertible.getRole())
                .lockdown(convertible.getLockdown())
                .build();
    }

    @Nullable
    @Override
    public User toModel(@Nullable final UserUnlimitedDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return User.builder()
                .id(convertible.getId())
                .login(convertible.getLogin())
                .passwordHash(convertible.getPasswordHash())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .role(convertible.getRole())
                .lockdown(convertible.getLockdown())
                .build();
    }

}