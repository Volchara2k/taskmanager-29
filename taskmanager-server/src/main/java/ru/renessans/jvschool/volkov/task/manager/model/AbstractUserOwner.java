package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString
@MappedSuperclass
public abstract class AbstractUserOwner extends AbstractModel {

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    private TimeFrame timeFrame = new TimeFrame();

    @NotNull
    @Column(nullable = false, columnDefinition = "TINYTEXT")
    private String title = "";

    @NotNull
    @Column(nullable = false, columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    @Column(nullable = false)
    private String userId;

}