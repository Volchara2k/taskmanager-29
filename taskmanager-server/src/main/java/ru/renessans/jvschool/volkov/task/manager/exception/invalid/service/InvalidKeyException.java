package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidKeyException extends AbstractException {

    @NotNull
    private static final String EMPTY_KEY = "Ошибка! Параметр \"ключ значения\" является пустым или null!\n";

    public InvalidKeyException() {
        super(EMPTY_KEY);
    }

}