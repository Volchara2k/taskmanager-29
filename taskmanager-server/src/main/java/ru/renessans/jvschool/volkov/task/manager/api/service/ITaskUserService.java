package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

public interface ITaskUserService extends IOwnerUserService<Task, ITaskUserRepository> {

    @NotNull
    Task add(
            @Nullable Task task
    );

    @NotNull
    Task add(
            @Nullable String userId,
            @Nullable String projectTitle,
            @Nullable String title,
            @Nullable String description
    );

}