package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointContextService;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class UserEndpointTest {

    @NotNull
    private final IEndpointContextRepository endpointLocatorRepository = new EndpointContextRepository();

    @NotNull
    private final IEndpointContextService endpointLocator = new EndpointContextService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = endpointLocator.getAuthenticationEndpoint();

    @NotNull
    private final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

    @NotNull
    private final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();

    @Test
    @TestCaseName("Run testGetUser for getUser(session)")
    public void testGetUser() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO getUser = this.userEndpoint.getUser(open);
        Assert.assertNotNull(getUser);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetUserRole for getUserRole(session)")
    public void testGetUserRole() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final SessionDTO open = sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final UserRole getRole = this.userEndpoint.getUserRole(open);
        Assert.assertNotNull(getRole);
        Assert.assertEquals(UserRole.ADMIN, getRole);
        Assert.assertEquals(UserRole.ADMIN, getRole);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfile(session, \"newFirstName\")")
    public void testEditProfile() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO editUser = this.userEndpoint.editProfile(open, "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(DemoDataConst.USER_TEST_LOGIN, editUser.getLogin());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testEditProfileWithLastName for editProfileWithLastName(session, \"newData\", \"newData\")")
    public void testEditProfileWithLastName() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO editUser = this.userEndpoint.editProfileWithLastName(open, "newData", "newData");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(DemoDataConst.USER_DEFAULT_LOGIN, editUser.getLogin());
        Assert.assertEquals("newData", editUser.getFirstName());
        Assert.assertEquals("newData", editUser.getLastName());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdatePassword for updatePassword(session, \"{2}\")")
    public void testUpdatePassword() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUser);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                login, password);
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO updatePassword = this.userEndpoint.updatePassword(open, "newPassword");
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(login, updatePassword.getLogin());
        Assert.assertEquals(addUser.getId(), updatePassword.getId());

        @NotNull final SessionDTO openAdminSession = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openAdminSession);
        @NotNull final UserLimitedDTO deleteUser = this.adminEndpoint.deleteUserByLogin(openAdminSession, login);
        Assert.assertNotNull(deleteUser);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(openAdminSession);
        Assert.assertNotNull(closed);
    }

}