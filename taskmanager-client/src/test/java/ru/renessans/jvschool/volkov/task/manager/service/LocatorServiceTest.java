package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;

@Category(IntegrationImplementation.class)
public final class LocatorServiceTest {

    @NotNull
    private final IServiceContextRepository serviceLocatorRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceLocator = new ServiceContextService(serviceLocatorRepository);

    @NotNull
    private final IEndpointContextRepository endpointLocatorRepository = new EndpointContextRepository();

    @NotNull
    private final IEndpointContextService endpointLocator = new EndpointContextService(endpointLocatorRepository);

    @NotNull
    private final IApplicationContextService locatorService = new ApplicationContextService(
            endpointLocator, serviceLocator
    );

    @Test
    @TestCaseName("Run testGetEndpointLocator for getEndpointLocator()")
    public void testGetEndpointLocator() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.locatorService);
        @NotNull final IEndpointContextService endpointLocatorService = this.locatorService.getEndpointContext();
        Assert.assertNotNull(endpointLocatorService);
    }

    @Test
    @TestCaseName("Run testGetServiceLocator for getServiceLocator()")
    public void testGetServiceLocator() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.locatorService);
        @NotNull final IServiceContextService serviceLocatorService = this.locatorService.getServiceContext();
        Assert.assertNotNull(serviceLocatorService);
    }

}