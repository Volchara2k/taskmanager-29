package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_INDEX =
            "Происходит попытка инициализации обновления задачи \n" +
                    "Для обновления задачи по индексу введите индекс задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final TaskDTO updatedTask = taskEndpoint.updateTaskByIndex(current, index, title, description);
        ViewUtil.print(updatedTask);
    }

}