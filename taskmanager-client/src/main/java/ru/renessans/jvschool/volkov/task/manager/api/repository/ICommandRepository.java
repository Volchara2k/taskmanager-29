package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getAllCommands();

    @NotNull
    Collection<AbstractCommand> getAllTerminalCommands();

    @NotNull
    Collection<AbstractCommand> getAllArgumentCommands();

    @Nullable
    AbstractCommand getArgumentCommand(@NotNull String command);

    @Nullable
    AbstractCommand getTerminalCommand(@NotNull String command);

}