package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProjectDeleteByIdCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_ID = "project-delete-by-id";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_ID = "удалить проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_ID =
            "Происходит попытка инициализации удаления проекта. \n" +
                    "Для удаления проекта по идентификатору введите идентификатор проекта из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final ProjectDTO project = projectEndpoint.deleteProjectById(current, id);
        ViewUtil.print(project);
    }

}