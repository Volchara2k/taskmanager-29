package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

public final class EndpointContextRepository implements IEndpointContextRepository {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();


    @NotNull
    private final AdminDataInterChangeEndpointService dataEndpointService = new AdminDataInterChangeEndpointService();

    @NotNull
    private final AdminDataInterChangeEndpoint dataEndpoint = dataEndpointService.getAdminDataInterChangeEndpointPort();


    @NotNull
    private final AuthenticationEndpointService authEndpointService = new AuthenticationEndpointService();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = authEndpointService.getAuthenticationEndpointPort();


    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();


    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();


    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    @Override
    public AuthenticationEndpoint getAuthenticationEndpoint() {
        return this.authEndpoint;
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return this.adminEndpoint;
    }

    @NotNull
    @Override
    public AdminDataInterChangeEndpoint getAdminDataInterChangeEndpoint() {
        return this.dataEndpoint;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return this.sessionEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return this.userEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return this.projectEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return this.taskEndpoint;
    }

}