package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Objects;

@SuppressWarnings("unused")
public final class ApplicationExitCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_EXIT = "exit";

    @NotNull
    private static final String DESC_EXIT = "закрыть приложение";

    @NotNull
    private static final String NOTIFY_EXIT = "Выход из приложения!";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_EXIT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_EXIT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_EXIT);
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();
        closeCurrentSession(current);
        System.exit(0);
    }

    private void closeCurrentSession(@Nullable final SessionDTO sessionDTO) {
        if (Objects.isNull(sessionDTO)) return;
        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        @Nullable final SessionDTO isClosed = sessionEndpoint.closeSession(sessionDTO);
        ViewUtil.print(isClosed);
    }

}