package ru.renessans.jvschool.volkov.task.manager.command.security;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_LOG_OUT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

        @Nullable final SessionDTO isClosed = sessionEndpoint.closeSession(current);
        @NotNull final SessionDTO unsubscribeSession = currentSessionService.unsubscribe();

        ViewUtil.print(NOTIFY_LOG_OUT);
        ViewUtil.print(unsubscribeSession);
    }

}