package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataBase64ImportCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_BASE64_IMPORT = "data-base64-import";

    @NotNull
    private static final String DESC_BASE64_IMPORT = "импортировать домен из base64 вида";

    @NotNull
    private static final String NOTIFY_BASE64_IMPORT = "Происходит процесс загрузки домена из base64 вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_IMPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESC_BASE64_IMPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        @NotNull final DomainDTO domain = dataEndpoint.importDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_IMPORT);
        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}